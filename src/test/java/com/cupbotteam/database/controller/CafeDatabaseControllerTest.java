package com.cupbotteam.database.controller;

import com.cupbotteam.database.core.Cafe;
import com.cupbotteam.database.core.CafeAddress;
import com.cupbotteam.database.core.CafeId;
import com.cupbotteam.database.repository.CafeRepository;
import com.cupbotteam.database.repository.PromoRepository;
import com.cupbotteam.database.service.DatabaseAuthentication;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CafeDatabaseController.class)
public class CafeDatabaseControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    DatabaseAuthentication databaseAuthentication;
    @MockBean
    CafeRepository cafeRepository;
    @MockBean
    PromoRepository promoRepository;
    ObjectWriter ow;

    @BeforeEach
    public void setUp() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ow = mapper.writer().withDefaultPrettyPrinter();
    }

    public void testAuth(MockHttpServletRequestBuilder uri) throws Exception {
        doReturn(true).when(databaseAuthentication).authenticateResponseToken(any(String.class));
        mockMvc.perform(uri).andExpect(status().isOk());

        doReturn(false).when(databaseAuthentication).authenticateResponseToken(any(String.class));
        mockMvc.perform(uri).andExpect(status().isForbidden());
    }

    public CafeId dummyCafeId() {
        CafeId cafeId = new CafeId();
        cafeId.setCafeName("ABCDE");
        cafeId.setCafeOwnerTwitterId(123);
        return cafeId;
    }

    public CafeAddress dummyCafeAddress() {
        CafeAddress cafeAddress = new CafeAddress();
        cafeAddress.setStreet("A");
        cafeAddress.setSubDistrict("B");
        cafeAddress.setDistrict("C");
        cafeAddress.setCity("D");
        cafeAddress.setProvince("E");
        return cafeAddress;
    }

    public Cafe dummyCafe() {
        Cafe cafe = new Cafe(dummyCafeId(), dummyCafeAddress());
        cafe.setCafeOfAllTimeRank(1);
        cafe.setCafeOfTheWeekWins(1);
        return cafe;
    }

    @Test
    public void testGetCafe() throws Exception {
        String requestJson = ow.writeValueAsString(dummyCafeId());
        testAuth(post("/cafe/getCafe")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testGetCafeByTwitterId() throws Exception {
        String requestJson = ow.writeValueAsString(Long.parseLong("123"));
        testAuth(post("/cafe/getCafeByOwnerTwitterId")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testGetAllCafe() throws Exception {
        testAuth(get("/cafe/getAllCafe")
                .queryParam("response", "ABCDE"));
    }

    @Test
    public void testAddCafe() throws Exception {
        String requestJson = ow.writeValueAsString(dummyCafe());
        testAuth(post("/cafe/addCafe")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testAddMultipleCafe() throws Exception {
        List<Cafe> cafeList = new ArrayList<>();
        cafeList.add(dummyCafe());
        cafeList.add(dummyCafe());
        String requestJson = ow.writeValueAsString(cafeList);
        testAuth(post("/cafe/addMultipleCafe")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testDeleteCafe() throws Exception {
        String requestJson = ow.writeValueAsString(dummyCafeId());
        testAuth(post("/cafe/deleteCafe")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void deleteMultipleCafe() throws Exception {
        List<CafeId> cafeIds = new ArrayList<>();
        cafeIds.add(dummyCafeId());
        cafeIds.add(dummyCafeId());
        String requestJson = ow.writeValueAsString(cafeIds);
        testAuth(post("/cafe/deleteMultipleCafe")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testDeleteAllCafe() throws Exception {
        testAuth(get("/cafe/deleteAllCafe")
                .queryParam("response", "ABCDE"));
    }
}


