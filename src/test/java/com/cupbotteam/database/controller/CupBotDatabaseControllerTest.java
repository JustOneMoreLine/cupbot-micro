package com.cupbotteam.database.controller;

import com.cupbotteam.database.repository.CafeRepository;
import com.cupbotteam.database.repository.PromoRepository;
import com.cupbotteam.database.service.DatabaseAuthentication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CupBotDatabaseController.class)
public class CupBotDatabaseControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    DatabaseAuthentication databaseAuthentication;
    @MockBean
    CafeRepository cafeRepository;
    @MockBean
    PromoRepository promoRepository;

    @Test
    public void testWakeUp() throws Exception {
        mockMvc.perform(get("/wakeMe"))
                .andExpect(status().isOk());
        verify(databaseAuthentication).checkOutdateTokens();
    }

    @Test
    public void testChallange() throws Exception {
        MvcResult result = mockMvc.perform(get("/challange")).andExpect(status().isOk()).andReturn();
        assertTrue(result.getResponse().getContentAsString().contains("token"));
    }
}
