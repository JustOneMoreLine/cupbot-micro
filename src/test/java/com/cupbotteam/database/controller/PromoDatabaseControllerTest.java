package com.cupbotteam.database.controller;

import com.cupbotteam.database.core.CafeAddress;
import com.cupbotteam.database.core.CafeId;
import com.cupbotteam.database.core.CafePromo;
import com.cupbotteam.database.repository.CafeRepository;
import com.cupbotteam.database.repository.PromoRepository;
import com.cupbotteam.database.service.DatabaseAuthentication;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = PromoDatabaseController.class)
public class PromoDatabaseControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    DatabaseAuthentication databaseAuthentication;
    @MockBean
    CafeRepository cafeRepository;
    @MockBean
    PromoRepository promoRepository;
    ObjectWriter ow;

    @BeforeEach
    public void setUp() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ow = mapper.writer().withDefaultPrettyPrinter();
    }

    public void testAuth(MockHttpServletRequestBuilder uri) throws Exception {
        doReturn(true).when(databaseAuthentication).authenticateResponseToken(any(String.class));
        mockMvc.perform(uri).andExpect(status().isOk());

        doReturn(false).when(databaseAuthentication).authenticateResponseToken(any(String.class));
        mockMvc.perform(uri).andExpect(status().isForbidden());
    }

    public CafeId dummyCafeId() {
        CafeId cafeId = new CafeId();
        cafeId.setCafeName("ABCDE");
        cafeId.setCafeOwnerTwitterId(123);
        return cafeId;
    }

    public CafeAddress dummyCafeAddress() {
        CafeAddress cafeAddress = new CafeAddress();
        cafeAddress.setStreet("A");
        cafeAddress.setSubDistrict("B");
        cafeAddress.setDistrict("C");
        cafeAddress.setCity("D");
        cafeAddress.setProvince("E");
        return cafeAddress;
    }

    public CafePromo dummyPromo() {
        CafePromo cafePromo = new CafePromo(
                123,
                dummyCafeId(),
                "ABCDE",
                LocalDateTime.now(),
                LocalDateTime.now().plusHours(1),
                "ISEEDEADBODY"
        );
        return cafePromo;
    }

    @Test
    public void testGetPromoById() throws Exception {
        String requestJson = ow.writeValueAsString(Long.parseLong("123"));
        testAuth(post("/promo/getPromoById")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testGetCafePromo() throws Exception {
        String requestJson = ow.writeValueAsString(dummyCafeId());
        testAuth(post("/promo/getCafePromo")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testGetAllPromo() throws Exception {
        String requestJson = ow.writeValueAsString(Long.parseLong("123"));
        testAuth(get("/promo/getAllPromo")
                .queryParam("response", "ABCDE"));
    }

    @Test
    public void testAddPromo() throws Exception {
        doReturn(dummyPromo()).when(promoRepository).saveAndFlush(any(CafePromo.class));
        String requestJson = ow.writeValueAsString(dummyCafeId());
        testAuth(post("/promo/addPromo")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testDeletePromoById() throws Exception {
        String requestJson = ow.writeValueAsString(Long.parseLong("123"));
        testAuth(post("/promo/deletePromoById")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testDeleteCafePromo() throws Exception {
        String requestJson = ow.writeValueAsString(dummyCafeId());
        testAuth(post("/promo/deleteCafePromo")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testDeleteMultiplePromoById() throws Exception {
        List<Long> ids = new ArrayList<>();
        ids.add(Long.parseLong("123"));
        ids.add(Long.parseLong("1234"));
        String requestJson = ow.writeValueAsString(ids);
        testAuth(post("/promo/deleteMultiplePromoById")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testDeleteMultipleCafePromo() throws Exception {
        List<CafeId> cafeIds = new ArrayList<>();
        cafeIds.add(dummyCafeId());
        cafeIds.add(dummyCafeId());
        String requestJson = ow.writeValueAsString(cafeIds);
        testAuth(post("/promo/deleteMultipleCafePromo")
                .queryParam("response", "ABCDE")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson));
    }

    @Test
    public void testDeleteAllPromo() throws Exception {
        testAuth(get("/promo/deleteAllPromo")
                .queryParam("response", "ABCDE"));
    }
}
