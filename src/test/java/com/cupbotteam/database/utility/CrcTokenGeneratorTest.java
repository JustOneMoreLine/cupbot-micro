package com.cupbotteam.database.utility;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CrcTokenGeneratorTest {

    @Test
    public void generateCrcToken() {
        String crcToken = CrcTokenGenerator.generateCrcToken();
        assertTrue(crcToken.length() == 10);
    }
}
