package com.cupbotteam.database.utility;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Sha256Test {

    @Test
    public void testHashing() {
        String test1 = Sha256.hash("Hello World");
        String test2 = Sha256.hash("Hello World");
        assertEquals(test1, test2);
    }
}
