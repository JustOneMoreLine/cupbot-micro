package com.cupbotteam.database.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CrcTokenTest {
    @Mock
    private CrcToken crcToken;

    @BeforeEach
    public void setUp() {
        crcToken = new CrcToken();
        crcToken.setToken("HelloToken");
    }

    @Test
    public void testSetter() {
       crcToken.setToken("Hi");
       assertTrue(crcToken.getToken().equals("Hi"));
    }

    @Test
    public void testGetter() {
        assertTrue(crcToken.getToken().equals("HelloToken"));
    }

    @Test
    public void testToString() {
        assertEquals(crcToken.toString(), "HelloToken");
    }
}
