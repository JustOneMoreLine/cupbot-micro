package com.cupbotteam.database.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CafeIdTest {

    @Test
    public void testToString() {
        CafeId a = new CafeId();
        a.setCafeOwnerTwitterId(123);
        a.setCafeName("ABC");
        CafeId b = new CafeId();
        b.setCafeOwnerTwitterId(123);
        b.setCafeName("ABC");
        CafeId c = new CafeId();
        c.setCafeOwnerTwitterId(222);
        c.setCafeName("AAA");

        assertTrue(a.equals(a));
        assertTrue(a.equals(b));
        assertFalse(a.equals(c));
    }
}
