package com.cupbotteam.database.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.LocalDateTime;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CafePromoTest {
    CafePromo cafePromo;
    LocalDateTime start;
    LocalDateTime end;

    @BeforeEach
    public void setUp() {
        start = LocalDateTime.now();
        end = LocalDateTime.now().plusHours(1);
        cafePromo = new CafePromo(
                123,
                dummyCafeId(),
                "ABCD",
                start,
                end,
                "ABCD"
        );
    }

    public CafeId dummyCafeId() {
        CafeId cafeId = new CafeId();
        cafeId.setCafeOwnerTwitterId(123);
        cafeId.setCafeName("ABCD");
        return cafeId;
    }

    @Test
    public void testCafePromo() {
        assertEquals(cafePromo.getId(), 123);
        cafePromo.setId(111);
        assertEquals(cafePromo.getId(), 111);

        assertEquals(cafePromo.getCafeId(), dummyCafeId());
        cafePromo.setCafeId(null);
        assertNull(cafePromo.getCafeId());

        assertEquals(cafePromo.getStartOfPromo(), start);
        LocalDateTime time = LocalDateTime.now();
        cafePromo.setStartOfPromo(time);
        assertEquals(cafePromo.getStartOfPromo(), time);

        assertEquals(cafePromo.getEndOfPromo(), end);
        time = LocalDateTime.now();
        cafePromo.setEndOfPromo(time);
        assertEquals(cafePromo.getEndOfPromo(), time);

        assertEquals(cafePromo.getPromoCode(), "ABCD");
        cafePromo.setPromoCode("AAA");
        assertEquals(cafePromo.getPromoCode(), "AAA");

        assertEquals(cafePromo.getPromoDescription(), "ABCD");
        cafePromo.setPromoDescription("AAA");
        assertEquals(cafePromo.getPromoDescription(), "AAA");

        assertEquals(cafePromo.getZoneId(), "GMT+7");
    }
}
