package com.cupbotteam.database.service;

import com.cupbotteam.database.utility.Sha256;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.time.LocalDateTime;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DatabaseAuthenticationTest {
    @Mock
    DatabaseAuthentication databaseAuthenticaion;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        databaseAuthenticaion = new DatabaseAuthenticationImpl();
    }

    @Test
    public void createCrcToken() {
        String token = databaseAuthenticaion.generateCrcToken();
        assertTrue(token.length() == 10);
    }

    @Test
    public void authenticateResponseToken() {
        String token = databaseAuthenticaion.generateCrcToken();
        String responseToken = Sha256.hash(token);
        assertTrue(databaseAuthenticaion.authenticateResponseToken(responseToken));
    }

    @Test
    public void authenticaeResponseTokenFail() {
        String token = databaseAuthenticaion.generateCrcToken();
        assertFalse(databaseAuthenticaion.authenticateResponseToken(token));
    }

    @Test
    public void testCheckOutdatedToken() {
        databaseAuthenticaion.addToken("ABC", LocalDateTime.now());
        databaseAuthenticaion.addToken("DEF", LocalDateTime.now().minusHours(3));
        assertTrue(databaseAuthenticaion.isTokenValid("ABC"));
        assertTrue(databaseAuthenticaion.isTokenValid("DEF"));
        databaseAuthenticaion.checkOutdateTokens();
        assertTrue(databaseAuthenticaion.isTokenValid("ABC"));
        assertFalse(databaseAuthenticaion.isTokenValid("DEF"));
    }
}
