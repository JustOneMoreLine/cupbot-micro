package com.cupbotteam.database.utility;

import java.util.Random;

public class CrcTokenGenerator {
    private static final String upperCaseStrings = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String lowerCaseStrings = "abcdefghijklmnopqrstuvwxyz";
    private static final String numbersStrings = "0123456789";
    private static final String alphanumericString = upperCaseStrings + lowerCaseStrings + numbersStrings;

    public static String generateCrcToken() {
        String crcResult = "";
        Random randint = new Random();
        for (int i = 0; i < 10; i++) {
            int random = randint.nextInt(alphanumericString.length());
            crcResult = crcResult.concat(alphanumericString.substring(random, random + 1));
        }
        return crcResult;
    }
}
