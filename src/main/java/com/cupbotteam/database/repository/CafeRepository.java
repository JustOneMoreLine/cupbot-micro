package com.cupbotteam.database.repository;

import com.cupbotteam.database.core.Cafe;
import com.cupbotteam.database.core.CafeId;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface CafeRepository extends JpaRepository<Cafe, CafeId> {
    //@Query("select u from cafe where u.id.cafeOwnerTwitterId = ?#{[0]}")
    List<Cafe> findCafesByIdCafeOwnerTwitterId(long cafeOwnerTwitterId);
}
