package com.cupbotteam.database.repository;

import com.cupbotteam.database.core.CafeId;
import com.cupbotteam.database.core.CafePromo;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface PromoRepository extends JpaRepository<CafePromo, Long> {
    List<CafePromo> findAllByCafeId(CafeId id);
}
