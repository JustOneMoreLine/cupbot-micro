package com.cupbotteam.database.controller;

import com.cupbotteam.database.core.Cafe;
import com.cupbotteam.database.core.CafePromo;
import com.cupbotteam.database.core.CrcToken;
import com.cupbotteam.database.repository.CafeRepository;
import com.cupbotteam.database.repository.PromoRepository;
import com.cupbotteam.database.service.DatabaseAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

@Controller
public class CupBotDatabaseController {
    private DatabaseAuthentication databaseAuthentication;
    private CafeRepository cafeRepository;
    private PromoRepository promoRepository;

    @Autowired
    public CupBotDatabaseController(
            DatabaseAuthentication databaseAuthentication,
            CafeRepository cafeRepository,
            PromoRepository promoRepository) {
        this.databaseAuthentication = databaseAuthentication;
        this.cafeRepository = cafeRepository;
        this.promoRepository = promoRepository;
    }

    @GetMapping("/")
    public String listRegisteredCafe(Model model) {
        List<Cafe> registeredCafes = cafeRepository.findAll();
        List<CafePromo> registeredPromos = promoRepository.findAll();
        model.addAttribute("cafes", registeredCafes);
        model.addAttribute("promos", registeredPromos);
        return "home";
    }

    @GetMapping("/wakeMe")
    public ResponseEntity wakeMe() {
        databaseAuthentication.checkOutdateTokens();
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/challange")
    @ResponseBody
    public CrcToken challange() {
        CrcToken token = new CrcToken();
        token.setToken(databaseAuthentication.generateCrcToken());
        return token;
    }
}
