package com.cupbotteam.database.controller;

import com.cupbotteam.database.core.CafeId;
import com.cupbotteam.database.core.CafePromo;
import com.cupbotteam.database.repository.PromoRepository;
import com.cupbotteam.database.service.DatabaseAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/promo")
public class PromoDatabaseController {
    PromoRepository promoRepository;
    DatabaseAuthentication databaseAuthentication;
    protected static boolean open;

    @Autowired
    public PromoDatabaseController(
            PromoRepository promoRepository,
            DatabaseAuthentication databaseAuthentication) {
        this.promoRepository = promoRepository;
        this.databaseAuthentication = databaseAuthentication;
        open = true;
    }

    @PostMapping("/getPromoById")
    @ResponseBody
    public ResponseEntity<List<CafePromo>> getPromoById(@RequestParam String response, @RequestBody long id) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<Long> listId = new ArrayList<>();
            listId.add(Long.valueOf(id));
            Iterable<Long> iterableId = listId;
            List<CafePromo> promo = promoRepository.findAllById(iterableId);
            return ResponseEntity.ok(promo);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/getCafePromo")
    @ResponseBody
    public ResponseEntity<List<CafePromo>> getCafePromo(
            @RequestParam String response,
            @RequestBody CafeId cafeId) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<CafePromo> cafePromoList = promoRepository.findAllByCafeId(cafeId);
            return ResponseEntity.ok(cafePromoList);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/getAllPromo")
    @ResponseBody
    public ResponseEntity<List<CafePromo>> getAllPromo(@RequestParam String response) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<CafePromo> cafePromoList = promoRepository.findAll();
            return ResponseEntity.ok(cafePromoList);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/addPromo")
    @ResponseBody
    public ResponseEntity<CafePromo> addPromo(
            @RequestParam String response,
            @RequestBody CafePromo cafePromo) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            CafePromo savedPromo = promoRepository.saveAndFlush(cafePromo);
            return ResponseEntity.ok(savedPromo);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/addMultiplePromo")
    @ResponseBody
    public ResponseEntity<List<CafePromo>> addMultiplePromo(
            @RequestParam String response,
            @RequestBody List<CafePromo> cafePromoList) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<CafePromo> savedPromo = new ArrayList<>();
            for (CafePromo promo : cafePromoList) {
                CafePromo saved = promoRepository.saveAndFlush(promo);
                savedPromo.add(saved);
            }
            return ResponseEntity.ok(savedPromo);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/deletePromoById")
    @ResponseBody
    public ResponseEntity<List<CafePromo>> deletePromoById(
            @RequestParam String response,
            @RequestBody long id) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<CafePromo> deletedPromo = new ArrayList<>();
            List<CafePromo> allPromo = promoRepository.findAll();
            for (CafePromo promo : allPromo) {
                if (promo.getId() == id) {
                    deletedPromo.add(promo);
                    promoRepository.delete(promo);
                }
            }
            return ResponseEntity.ok(deletedPromo);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/deleteCafePromo")
    @ResponseBody
    public ResponseEntity<List<CafePromo>> deleteCafePromo(
            @RequestParam String response,
            @RequestBody CafeId cafeId) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<CafePromo> deletedPromo = new ArrayList<>();
            List<CafePromo> knownPromo = promoRepository.findAllByCafeId(cafeId);
            for (CafePromo promo : knownPromo) {
                deletedPromo.add(promo);
                promoRepository.delete(promo);
            }
            return ResponseEntity.ok(deletedPromo);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/deleteMultiplePromoById")
    @ResponseBody
    public ResponseEntity<List<CafePromo>> deleteMultiplePromoById(
            @RequestParam String response,
            @RequestBody List<Long> ids) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<CafePromo> deletedPromo = new ArrayList<>();
            Iterable<Long> iterableId = ids;
            List<CafePromo> knownPromo = promoRepository.findAllById(iterableId);
            for (CafePromo promo : knownPromo) {
                deletedPromo.add(promo);
                promoRepository.delete(promo);
            }
            return ResponseEntity.ok(deletedPromo);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/deleteMultipleCafePromo")
    @ResponseBody
    public ResponseEntity<List<CafePromo>> deleteMultipleCafePromo(
            @RequestParam String response,
            @RequestBody List<CafeId> idList) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<CafePromo> deletedPromo = new ArrayList<>();
            List<CafePromo> knownPromo = new ArrayList<>();
            for (CafeId id : idList) {
                knownPromo.addAll(promoRepository.findAllByCafeId(id));
            }
            for (CafePromo promo : knownPromo) {
                deletedPromo.add(promo);
                promoRepository.delete(promo);
            }
            return ResponseEntity.ok(deletedPromo);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/deleteAllPromo")
    @ResponseBody
    public ResponseEntity<List<CafePromo>> deleteAllPromo(@RequestParam String response) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<CafePromo> deletedPromo = new ArrayList<>();
            List<CafePromo> allPromo = promoRepository.findAll();
            for (CafePromo promo : allPromo) {
                deletedPromo.add(promo);
                promoRepository.delete(promo);
            }
            return ResponseEntity.ok(deletedPromo);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }
}
