package com.cupbotteam.database.controller;

import com.cupbotteam.database.core.Cafe;
import com.cupbotteam.database.core.CafeId;
import com.cupbotteam.database.repository.CafeRepository;
import com.cupbotteam.database.service.DatabaseAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/cafe")
public class CafeDatabaseController {
    CafeRepository cafeRepository;
    DatabaseAuthentication databaseAuthentication;
    protected static boolean open;

    @Autowired
    public CafeDatabaseController(CafeRepository cafeRepository, DatabaseAuthentication databaseAuthentication) {
        this.cafeRepository = cafeRepository;
        this.databaseAuthentication = databaseAuthentication;
        open = true;
    }

    @PostMapping("/getCafe")
    @ResponseBody
    public ResponseEntity<List<Cafe>> getCafe(
            @RequestParam String response,
            @RequestBody CafeId cafeId) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            ArrayList<CafeId> idArrayList = new ArrayList<>();
            idArrayList.add(cafeId);
            Iterable<CafeId> iterableId = idArrayList;
            List<Cafe> getCafe = cafeRepository.findAllById(iterableId);
            return ResponseEntity.ok(getCafe);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/getCafeByOwnerTwitterId")
    @ResponseBody
    public ResponseEntity<List<Cafe>> getCafeByTwitterId(
            @RequestParam String response,
            @RequestBody long cafeOwnerTwitterId) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<Cafe> getCafe = cafeRepository.findCafesByIdCafeOwnerTwitterId(cafeOwnerTwitterId);
            System.out.println("=========================");
            for (Cafe in : getCafe) {
                System.out.println(in.getId().getCafeOwnerTwitterId());
            }
            return ResponseEntity.ok(getCafe);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/getAllCafe")
    @ResponseBody
    public ResponseEntity<List<Cafe>> getAllCafe(@RequestParam String response) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<Cafe> allCafeInDatabase = cafeRepository.findAll();
            System.out.println("=========================");
            for (Cafe in : allCafeInDatabase) {
                System.out.println(in.getId().getCafeOwnerTwitterId());
            }
            return ResponseEntity.ok(allCafeInDatabase);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/addCafe")
    @ResponseBody
    public ResponseEntity<Cafe> addCafe(
            @RequestParam String response,
            @RequestBody Cafe cafe) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            cafeRepository.saveAndFlush(cafe);
            Cafe newCafe = cafeRepository.getOne(cafe.getId());
            return ResponseEntity.ok(newCafe);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/addMultipleCafe")
    @ResponseBody
    public ResponseEntity<List<Cafe>> addMultipleCafe(@RequestParam String response,
                                                      @RequestBody List<Cafe> listOfCafe) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<Cafe> savedCafe = new ArrayList<Cafe>();
            for (Cafe cafe : listOfCafe) {
                CafeId cafeId = cafe.getId();
                cafeRepository.saveAndFlush(cafe);
                savedCafe.add(cafeRepository.getOne(cafeId));
            }
            return ResponseEntity.ok(savedCafe);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/deleteCafe")
    @ResponseBody
    public ResponseEntity<List<Cafe>> deleteCafe(@RequestParam String response, @RequestBody CafeId cafeId) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<Cafe> deletedCafe = new ArrayList<>();
            ArrayList<CafeId> idArrayList = new ArrayList<>();
            idArrayList.add(cafeId);
            Iterable<CafeId> iterableId = idArrayList;
            List<Cafe> knownCafe = cafeRepository.findAllById(iterableId);
            for (Cafe cafe : knownCafe) {
                System.out.println("KNOWN CAFE: " + cafe.getId().getCafeName());
                if (cafe.getId().equals(cafeId)) {
                    System.out.println("DELETE THIS: " + cafeId.getCafeName());
                    deletedCafe.add(cafe);
                    cafeRepository.delete(cafe);
                }
            }
            System.out.println("=========================");
            for (Cafe cafe : deletedCafe) {
                System.out.println(cafe.getId().getCafeName());
            }
            System.out.println("=========================");
            return ResponseEntity.ok(deletedCafe);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/deleteMultipleCafe")
    @ResponseBody
    public ResponseEntity<List<Cafe>> deleteMultipleCafe(@RequestParam String response,
                                                         @RequestBody List<CafeId> idList) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<Cafe> deletedCafes = new ArrayList<>();
            List<Cafe> knownCafe = new ArrayList<>();
            Iterable<CafeId> iterableId = idList;
            knownCafe.addAll(cafeRepository.findAllById(iterableId));
            for (Cafe cafe : knownCafe) {
                deletedCafes.add(cafe);
                cafeRepository.delete(cafe);
            }
            return ResponseEntity.ok(deletedCafes);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/deleteAllCafe")
    @ResponseBody
    public ResponseEntity<List<Cafe>> deleteAllCafe(@RequestParam String response) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            List<Cafe> deletedCafe = cafeRepository.findAll();
            cafeRepository.deleteAll();
            return ResponseEntity.ok(deletedCafe);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/incrementCotwWins")
    @ResponseBody
    public ResponseEntity<List<Cafe>> incrementCotwWins(
            @RequestParam String response,
            @RequestBody CafeId cafeId) {
        if (databaseAuthentication.authenticateResponseToken(response) && open) {
            try {
                ArrayList<CafeId> idArrayList = new ArrayList<>();
                idArrayList.add(cafeId);
                Iterable<CafeId> iterableId = idArrayList;
                List<Cafe> getCafe = cafeRepository.findAllById(iterableId);
                long currentWins = getCafe.get(0).getCafeOfTheWeekWins();
                getCafe.get(0).setCafeOfTheWeekWins(currentWins + 1);
                cafeRepository.save(getCafe.get(0));
                return ResponseEntity.ok(getCafe);
            } catch (Exception e) {
                return ResponseEntity.badRequest().build();
            }
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }
}
