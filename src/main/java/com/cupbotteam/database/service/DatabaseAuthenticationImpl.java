package com.cupbotteam.database.service;

import com.cupbotteam.database.utility.CrcTokenGenerator;
import com.cupbotteam.database.utility.Sha256;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

@Service
public class DatabaseAuthenticationImpl implements DatabaseAuthentication {
    private HashMap<String, LocalDateTime> generatedTokens = new HashMap<String, LocalDateTime>();

    @Override
    public String generateCrcToken() {
        String crcToken = CrcTokenGenerator.generateCrcToken();
        if (generatedTokens.containsKey(crcToken)) {
            return generateCrcToken();
        }
        generatedTokens.put(crcToken, LocalDateTime.now());
        return crcToken;
    }

    @Override
    public boolean authenticateResponseToken(String responseToken) {
        for (String token : generatedTokens.keySet()) {
            if (responseToken.equals(Sha256.hash(token))) {
                generatedTokens.remove(token);
                return true;
            }
        }
        return false;
    }

    @Override
    public void addToken(String token, LocalDateTime time) {
        generatedTokens.put(token, time);
    }

    @Override
    public boolean isTokenValid(String token) {
        if (generatedTokens.containsKey(token)) {
            return true;
        }
        return false;
    }

    @Override
    public void checkOutdateTokens() {
        LocalDateTime now = LocalDateTime.now();
        for (String token : generatedTokens.keySet()) {
            LocalDateTime tokenCreationDate = generatedTokens.get(token);
            if (tokenCreationDate.until(now, ChronoUnit.HOURS) > 2) {
               generatedTokens.remove(token);
            }
        }
    }
}
