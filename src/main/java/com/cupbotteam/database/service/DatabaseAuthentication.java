package com.cupbotteam.database.service;

import java.time.LocalDateTime;

public interface DatabaseAuthentication {
    public String generateCrcToken();
    public boolean authenticateResponseToken(String responseToken);
    public void checkOutdateTokens();
    public void addToken(String token, LocalDateTime time);
    public boolean isTokenValid(String token);
}
