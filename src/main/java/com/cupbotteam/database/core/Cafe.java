package com.cupbotteam.database.core;

import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Table(name = "cafe")
@Entity
public class Cafe {
    @EmbeddedId
    @NotNull
    private CafeId id;
    @Embedded
    @NotNull
    private CafeAddress cafeAddress;
    @Column
    private long cafeOfTheWeekWins = 0;
    @Column
    private long cafeOfAllTimeRank = Long.MAX_VALUE;

    public Cafe(CafeId id, CafeAddress cafeAddress) {
        this.id = id;
        this.cafeAddress = cafeAddress;
        this.cafeOfTheWeekWins = 0;
        this.cafeOfAllTimeRank = Long.MAX_VALUE;
    }

    public void setId(CafeId id) {
        this.id = id;
    }

    public CafeId getId() {
        return id;
    }

    public void setCafeAddress(CafeAddress cafeAddress) {
        this.cafeAddress = cafeAddress;
    }

    public CafeAddress getCafeAddress() {
        return cafeAddress;
    }

    public void setCafeOfTheWeekWins(long cafeOfTheWeekWins) {
        this.cafeOfTheWeekWins = cafeOfTheWeekWins;
    }

    public long getCafeOfTheWeekWins() {
        return cafeOfTheWeekWins;
    }

    public void setCafeOfAllTimeRank(long cafeOfAllTimeRank) {
        this.cafeOfAllTimeRank = cafeOfAllTimeRank;
    }

    public long getCafeOfAllTimeRank() {
        return cafeOfAllTimeRank;
    }
}
