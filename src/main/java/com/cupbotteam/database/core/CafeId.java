package com.cupbotteam.database.core;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class CafeId implements Serializable {
    @NotNull
    private long cafeOwnerTwitterId;
    @NotNull
    private String cafeName;

    public void setCafeOwnerTwitterId(long cafeOwnerTwitterId) {
        this.cafeOwnerTwitterId = cafeOwnerTwitterId;
    }

    public long getCafeOwnerTwitterId() {
        return cafeOwnerTwitterId;
    }

    public void setCafeName(String cafeName) {
        this.cafeName = cafeName;
    }

    public String getCafeName() {
        return cafeName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CafeId)) {
            return false;
        }

        CafeId obj1 = (CafeId) obj;
        if (this.getCafeOwnerTwitterId() == obj1.getCafeOwnerTwitterId() &&
        this.getCafeName().equals(obj1.getCafeName())) {
            return true;
        }
        return false;
    }
}
