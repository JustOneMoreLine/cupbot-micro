package com.cupbotteam.database.core;

import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Table(name = "cafePromo")
@Entity
public class CafePromo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    @NotNull
    @Embedded
    @Column
    private CafeId cafeId;
    @Column
    private String promoCode;
    @Column
    @NotNull
    private  LocalDateTime startOfPromo;
    @Column
    @NotNull
    private LocalDateTime endOfPromo;
    @Column
    @NotNull
    private String promoDescription;
    final String zoneId = "GMT+7";

    public CafePromo(
            long id,
            CafeId cafeId,
            String promoCode,
            LocalDateTime startOfPromo,
            LocalDateTime endOfPromo,
            String promoDescription) {
        this.id = id;
        this.cafeId = cafeId;
        this.promoCode = promoCode;
        this.startOfPromo = startOfPromo;
        this.endOfPromo = endOfPromo;
        this.promoDescription = promoDescription;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setCafeId(CafeId cafeId) {
        this.cafeId = cafeId;
    }

    public CafeId getCafeId() {
        return cafeId;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setStartOfPromo(LocalDateTime startOfPromo) {
        this.startOfPromo = startOfPromo;
    }

    public LocalDateTime getStartOfPromo() {
        return startOfPromo;
    }

    public void setEndOfPromo(LocalDateTime endOfPromo) {
        this.endOfPromo = endOfPromo;
    }

    public LocalDateTime getEndOfPromo() {
        return endOfPromo;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public String getZoneId() {
        return zoneId;
    }
}
