package com.cupbotteam.database.core;

public class CrcToken {
    private String token;

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }

    public String toString() {
        return this.token;
    }
}
