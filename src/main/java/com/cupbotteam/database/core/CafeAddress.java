package com.cupbotteam.database.core;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class CafeAddress {
    @NotNull
    private String street;
    @NotNull
    private String subDistrict; // kelurahan
    @NotNull
    private String district; // kecamatan
    @NotNull
    private String city;
    @NotNull
    private String province;

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return street;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDistrict() {
        return district;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvince() {
        return province;
    }
}
